const isGabbyPalindrome = (str) => {
  let reverse = "";
  for (let i = str.length - 1; i >= 0; i--) {
    reverse += str[i];
  }
  return reverse === str;
};
const cleanString = (s) => {
  return s
    .split("")
    .filter((char) => char !== " ")
    .join("");
};

const isPalindrome = (str) => {
  return cleanString(str) === cleanString(str).split("").reverse().join("");
};

// const people = [
//   { name: "Vince", age: 200 },
//   { name: "Tj", age: 300 },
//   { name: "Gabby", age: 400 },
// ];

// console.log(people.filter((person) => person.age < 350));
// console.log([1, 2, 3, 4].filter((num) => num < 3));
console.log(isPalindrome("mom")); // should print true
console.log(isPalindrome("papa")); // should print false
console.log(isPalindrome("nurses run")); // should print true

"
// const someObj = {
//   name: "Vince",
//   age: 2000,
//   numPets: 0,
//   key: "",
// };

// const { name, ...rest } = someObj;
// console.log(name, rest);
// delete someObj.name;
// console.log("keys bruh", Object.keys(someObj));
// console.log("values bruh", Object.values(someObj));

function removeKeys(obj, ...keys) {
  let nextObj = {};
  Object.keys(obj).forEach((keyName) => {
    if (!keys.includes(keyName)) {
      nextObj[keyName] = obj[keyName];
    }
  });

  return nextObj;
  // const copy = { ...obj };
  // keys.forEach((key) => {
  //   delete copy[key];
  // });
  // return copy;
}

// TODO Object.assign({}, obj);
// console.log("Obj before", someObj);
// console.log(removeKeys(someObj, "apple", "orange", "age", "key"));
// console.log("Obj after", someObj);

// console.log("apple", "orange", "age", "key");

// const someNumbers = [1, 2, 44, 3, 4, 5, 6];
// console.log(Math.max(...someNumbers));
// Math.max(1, 2, 44, 3, 4, 5, 6)
/*
----------- loops

// while (true) { }
// do {} while();
// foreach
// for of
// for in
// for

const people = ["Tj", "Sondos", "Suri", "Trevor"];

let numTimes = 0;
// This will run for every item regardless of if you need it to terminate
// Be very wise with this.
people.forEach((person) => {
  numTimes++;
  console.log(numTimes);
  if (person === "Suri") {
    return 2;
  }
});

  ----- functions and scope ----
function adder(number) {
  function doTheAddition(num) {
    return num + number;
  }

  return doTheAddition;
}

let addByTen = adder(10);
let addByTwo = adder(2);

console.log(addByTen(5), addByTwo(8)); // logs 15 and 10

*/

// const someArray = [1, 2, 3];
// const someObj = {
//   name: "Vince",
//   age: 2000,
//   numPets: 0,
//   key: "",
// };

// function hasKey(obj, key) {
//   if (typeof obj[key] !== "undefined") {
//     return true;
//   }
//   return false;
// }

// console.log(hasKey(someObj, "numPets"));
// console.log(typeof []); // Object, duh....

/* Functions

function someFunc() {}
console.log(someFunc);
const arrowFunc = () => 1;
let another = function () {};
let other = () => {};
*/
