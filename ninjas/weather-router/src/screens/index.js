import React from "react";
import { Switch, Route } from "react-router-dom";
import { FilterScreen } from "./FilterScreen";
import { HomeScreen } from "./HomeScreen";

export const Screens = ({ locations, filter }) => (
  <Switch>
    <Route path="/filtered">
      <FilterScreen locations={locations} filter={filter} />
    </Route>
    <Route exact path="/">
      <HomeScreen locations={locations} />
    </Route>
  </Switch>
);
