import React from "react";
import { Segment } from "@fluentui/react-northstar";

export const Footer = () => (
  <Segment
    color="brand"
    content="Footer Bro"
    inverted
    styles={{
      gridColumn: "span 4",
    }}
  />
);
