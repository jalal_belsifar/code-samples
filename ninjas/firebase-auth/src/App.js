import React, { useEffect, useState } from 'react';
import firebase from './firebase';
import './App.css';

function App() {
  const [email, setEmail] = useState("")
  const [password, setPass] = useState("")
  const [loggedInUser, setUser] = useState(null)

  useEffect(() => {
    const authListener = firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        console.log(user)
        setUser(user)
      } else {
        console.log("User Logged Out")
        setUser(null)
      }
    })

    return () => {
      authListener()
    }
  }, [])

  const registerUser = () => {
    firebase.auth().createUserWithEmailAndPassword(email, password)
      .catch(e => {
        alert(e)
      })
    
    setEmail("")
    setPass("")
  }

  const loginUser = () => {
    firebase.auth().signInWithEmailAndPassword(email, password)
      .catch(e => {
        alert(e)
      })
    
      setEmail("")
      setPass("")
  }

  return (
    <div className="App">
      <input type="email" placeholder="Email" value={email} onChange={e => setEmail(e.target.value)} />
      <input type="password" placeholder="Password" value={password} onChange={e => setPass(e.target.value)} />
      { loggedInUser ?
        <button onClick={() => firebase.auth().signOut()}>Logout</button> :
        <div>
          <button onClick={loginUser}>Sign In</button>
          <button onClick={registerUser}>Register</button>
        </div>}
      <p>{ loggedInUser ? JSON.stringify(loggedInUser) : ""}</p>
    </div>
  );
}

export default App;
