import React, { useEffect, useRef, useMemo } from "react";
import { useCoolCounter } from "./useCounter";
import usePeople from "./useSwapi";
import { calculateExpensiveFunction } from "./utils";

const Swapi = () => {
  const coolCounter = useCoolCounter(10);
  const inputRef = useRef(null);
  const buttonRef = useRef(null);
  const { people } = usePeople();
  const a = 10;
  const b = 20;

  const someValue = useMemo(() => calculateExpensiveFunction(a, b), [a, b]);

  useEffect(() => {
    // this will happen on mount
    if (inputRef.current) {
      // focus the input element
      inputRef.current.focus();
    }
  }, []);

  const doSomethingCool = () => {
    coolCounter.reduceCoolFactor(10);
  };

  // Even though "coolCounter.count" is incrementing in the background
  // and it is causing a "re-render"
  // this component will not update the UI until "people" changes
  // because we are wrapping our JS in a useMemo function
  // that only re-computes when the dependency array (second parameter) changes
  return useMemo(
    () => (
      <>
        <h2>Hey from Swapi</h2>
        <input ref={inputRef} type="text" name="something" />
        <p>{`${a} + ${b} = ${someValue} but I am this cool --> ${coolCounter.count}`}</p>
        <button ref={buttonRef} onClick={doSomethingCool}>
          You're not that cool Vince
        </button>

        <ul>
          {people.map((person) => (
            <li key={person.name}>{person.name}</li>
          ))}
        </ul>
      </>
    ),
    // This memoization will recompute if people or someValue changes
    [people, someValue],
  );
};

export default Swapi;
