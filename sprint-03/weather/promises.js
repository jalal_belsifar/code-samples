// (function invokeMe(name) {
//   return;
//   // console.log(`waddup ${name}`);

//   const somePromise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//       console.log("Set Timeout is done");
//       resolve("we are resolved");
//     }, 1000);
//     return 5;
//   });

// somePromise
// .then((val) => console.log({ val }))
// .then((val) => {
//   console.log({ val });
//   return 5;
// })
// .then(resolveMeBruh);
// somePromise.then(resolveMeBruh);
// console.log("somePromise is", somePromise);
// })("Tj");

// IIFE

// function resolveMeBruh(someValue) {
//   console.log({ someValue });
// }

// const something = new Promise((res, rej) => {
//   setTimeout(() => {
//     res(1);
//   }, 100);
// });

// function logAndAddFour(val) {
//   console.log({ val });
//   return val + 4;
// }

// something
//   .then(logAndAddFour)
//   .then(logAndAddFour)
//   .then(logAndAddFour)
//   .then(logAndAddFour)
//   .then(logAndAddFour)
//   .then(logAndAddFour)
//   .then(logAndAddFour)
//   .then(logAndAddFour)
//   .catch((err) => {
//     console.log("something broke", err);
//   })
//   .finally(() => {
//     console.log("I will always run last");
//   });

// const examplePromise = {
//   then() {},
//   catch() {},
//   finally() {},
// };

// console.log({ something });

// const aValue = console.log("hi");
// console.log("console returns", aValue);

// const named = { person: "Cesar", likesFood: false, cars: 1000 };
// console.log("all the keys of named", Object.keys(named));
// console.log("all the values of named", Object.values(named));

// const things = ["apple", "banana", "orange"];
// console.log("Last thing", things[things.length - 1]);
// console.log("Last thing", things.reverse()[0]);

// const things = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

// console.log(
//   "things squared",
//   things.map((thing) => thing * thing),
// );

// console.log(
//   "things < 5",
//   things.filter((thing) => thing < 5),
// );

// Promise.resolve(0).then((val) => console.log(val)); // #5
// console.log(1); // #1
// console.log(2); // #2

// const sto = setTimeout(() => {
//   console.log(3); // #6
// }, 0);
// clearTimeout(sto);

// console.log(4); // #3

// let timer = setInterval(() => {
//   console.log(5); // # wtf??
// }, 1000);
// console.log(6); // #4
// clearInterval(timer);
