import express from "express";
import _ from "lodash";
import * as db from "../database.js";
import { User } from "../models/sequelize/index.js";

export default (app) => {
  const router = express.Router();

  router.get("/postgres", async (req, res, next) => {
    try {
      const data = await User.findAll();
      res.json(data);
    } catch (err) {
      req.log.error(err.message);
      res.status(500).send("Internal Server Error");
    }
  });

  router.get("/:entity", async (req, res, next) => {
    try {
      const data = await db.readEntity(req.params.entity);
      const dataToSendBack = req.query ? _.filter(data, req.query) : data;
      res.json(dataToSendBack);
    } catch (err) {
      req.log.error(err.message);
      res.status(500).send("Internal Server Error");
    }
  });

  router.put("/:entity/:entityId", async (req, res) => {
    try {
      const data = await db.updateEntity(
        req.params.entity,
        req.params.entityId,
        req.body
      );
      res.json(data);
    } catch (err) {
      req.log.error(err.message);
      res.status(500).send("Internal Server Error");
    }
  });

  router.post("/:entity", async (req, res) => {
    try {
      const data = await db.createEntity(req.params.entity, req.body);
      res.status(201).json(data);
    } catch (err) {
      req.log.error(err.message);
      res.status(500).send("Internal Server Error");
    }
  });

  router.delete("/:entity/:entityId", async (req, res) => {
    try {
      await db.deleteEntity(req.params.entity, req.params.entityId);
      res.send("ok!");
    } catch (err) {
      req.log.error(err.message);
      res.status(500).send("Internal Server Error");
    }
  });

  app.use("/api", router);
};
