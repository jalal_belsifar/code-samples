import { combineReducers } from "redux";
import people from "./people";
import counter from "./counter";

export default combineReducers({
  people,
  counter,
});

// {
//   counter: 0,
//   people: [],
// }
