import { connect } from "react-redux";

// Give me the entire redux state in the parameter to this function
const mapStateToProps = (state, ownProps) => {
  // I will return some props here (specifically a count prop that comes from the redux counter state)
  return {
    count: (ownProps.multiplyBy || 1) * state.counter,
  };
};

// dispatching plain actions
const mapDispatchToProps = (dispatch, ownProps) => ({
  increment: () => dispatch({ type: "INCREMENT" }),
  decrement: () => dispatch({ type: "DECREMENT" }),
  reset: () => dispatch({ type: "RESET" }),
});

// connect our state to as an enhancer
export const enhancer = connect(mapStateToProps, mapDispatchToProps);
